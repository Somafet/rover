﻿using Somafet.Rover.Impl;
using Somafet.Rover.Impl.Models;

var rover = new Rover();
var roverController = new RoverController(rover);

// Note: need to kill to exit
while (true)
{
    try
    {
        var command = AskForCommand();
        roverController.InputCommand(command);

        Console.WriteLine(roverController.ControlledRover.ToString() + '\n');
    }
    catch (InvalidCommandException)
    {
        Console.WriteLine($"Invalid command given. Try again.");
    }
}


string AskForCommand()
{
    Console.WriteLine("Enter command for the rover:");
    var command = Console.ReadLine();

    return command ?? throw new InvalidCommandException();
}

