﻿namespace Somafet.Rover.Impl.Utils;

public enum MovementCommand
{
    Forward = 1,
    Backward = -1
}