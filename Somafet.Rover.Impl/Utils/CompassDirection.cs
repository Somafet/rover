﻿namespace Somafet.Rover.Impl.Utils;

public enum CompassDirection
{
    North,
    South,
    West,
    East
}