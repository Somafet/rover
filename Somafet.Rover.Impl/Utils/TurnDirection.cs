﻿namespace Somafet.Rover.Impl.Utils;

public enum TurnCommand
{
    Left,
    Right
}