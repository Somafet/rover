﻿using Somafet.Rover.Impl.Utils;

namespace Somafet.Rover.Impl.Models;

public class Rover
{
    private int _x;
    private int _y;
    private CompassDirection _compassDirection;

    public Rover()
    {
        _x = 0;
        _y = 0;
        _compassDirection = CompassDirection.North;
    }

    public Rover(int x, int y, CompassDirection compassDirection)
    {
        _x = x;
        _y = y;
        _compassDirection = compassDirection;
    }

    public int X => _x;

    public int Y => _y;

    public CompassDirection CompassDirection => _compassDirection;

    /// <summary>
    /// Change the rover's facing direction by the specified turn direction.
    /// </summary>
    /// <param name="turnCommand">The direction in which the rover should turn.</param>
    /// <exception cref="NotImplementedException">TODO: Implement rover direction change.</exception>
    public void Turn(TurnCommand turnCommand)
    {
        switch (turnCommand)
        {
            case TurnCommand.Left when _compassDirection == CompassDirection.North:
                _compassDirection = CompassDirection.West;
                break;
            case TurnCommand.Left when _compassDirection == CompassDirection.West:
                _compassDirection = CompassDirection.South;
                break;
            case TurnCommand.Left when _compassDirection == CompassDirection.South:
                _compassDirection = CompassDirection.East;
                break;
            case TurnCommand.Left when _compassDirection == CompassDirection.East:
                _compassDirection = CompassDirection.North;
                break;
            case TurnCommand.Right when _compassDirection == CompassDirection.North:
                _compassDirection = CompassDirection.East;
                break;
            case TurnCommand.Right when _compassDirection == CompassDirection.East:
                _compassDirection = CompassDirection.South;
                break;
            case TurnCommand.Right when _compassDirection == CompassDirection.South:
                _compassDirection = CompassDirection.West;
                break;
            case TurnCommand.Right when _compassDirection == CompassDirection.West:
                _compassDirection = CompassDirection.North;
                break;
        }
    }

    /// <summary>
    /// Move the rover in one of the possible movement directions.
    /// </summary>
    /// <param name="movementCommand">The movement command.</param>
    /// <exception cref="NotImplementedException">TODO: Implement rover movement.</exception>
    public void Move(MovementCommand movementCommand)
    {
        switch (_compassDirection)
        {
            case CompassDirection.North:
                _y += (int) movementCommand;
                break;
            case CompassDirection.South:
                _y -= (int) movementCommand;
                break;
            case CompassDirection.East:
                _x += (int) movementCommand;
                break;
            case CompassDirection.West:
                _x -= (int) movementCommand;
                break;
        }
    }

    public override string ToString()
    {
        return $"Rover {{ X: {_x}, Y: {_y}, Direction: {_compassDirection.ToString()} }}";
    }
}