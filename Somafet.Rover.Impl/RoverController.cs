﻿using Somafet.Rover.Impl.Utils;

namespace Somafet.Rover.Impl;

using Impl.Models;

public class RoverController
{
    private Rover _controlledRover;

    public const char Left = 'L';
    public const char Right = 'R';
    public const char Forward = 'F';
    public const char Backward = 'B';

    public RoverController(Rover controlledRover)
    {
        _controlledRover = controlledRover;
    }

    // Consider using transactional commands? (Command either succeeds or fails completely)
    public void InputCommand(ReadOnlySpan<char> commandString)
    {
        foreach (var c in commandString)
        {
            switch (c)
            {
                case Left:
                    _controlledRover.Turn(TurnCommand.Left);
                    break;
                case Right:
                    _controlledRover.Turn(TurnCommand.Right);
                    break;
                case Forward:
                    _controlledRover.Move(MovementCommand.Forward);
                    break;
                case Backward:
                    _controlledRover.Move(MovementCommand.Forward);
                    break;
                default:
                    throw new InvalidCommandException();
            }
        }
    }

    public Rover ControlledRover => _controlledRover;
}