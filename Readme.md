# Mars Rover Project

This project is responsible for the movement and turning of the Pluto Rover. It performs movements based on a given command input.

### Prerequisites

 - .NET SDK 6.0 + .NET CLI

### How to publish for use (Windows)

  - Pull the project through git then navigate to the `Somafet.Rover.Impl` folder inside the project root.
  - Run `dotnet publish`.
  - Navigate to `{PROJECT_ROOT}/Somafet.Rover.Impl/bin/Debug/net6.0/publish`.
  - Run the application Exe `Somafet.Rover.Impl.exe`.

For further options, like naming the application and cross-platform deployments, visit the [Microsoft documentation on publishing and deploying](https://docs.microsoft.com/en-us/dotnet/core/deploying/).

### How to test
 - After pulling, navigate to the `Somafet.Rover.Tests` directory.
 - Run the `dotnet test` command.

### Project Architecture

The project architecture file can be found at `{PROJECT_ROOT}/project_architecture.drawio`.