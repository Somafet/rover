namespace Somafet.Rover.Tests;

using NUnit.Framework;
using Impl.Utils;
using Impl.Models;



public class Tests
{
    private Rover _rover = null!;
    
    [SetUp]
    public void Setup()
    {
        _rover = new Rover();
    }

    [Test]
    public void DefaultInitialPositionIsSetAndCorrect()
    {
        Assert.AreEqual(CompassDirection.North, _rover.CompassDirection);
        Assert.AreEqual(0, _rover.X);
        Assert.AreEqual(0, _rover.Y);
    }
    
    [Test]
    [TestCase(0, 1, CompassDirection.East)]
    [TestCase(11, 2, CompassDirection.North)]
    public void CustomInitialPositionIsSetAndCorrect(int x, int y, CompassDirection compassDirection)
    {
        _rover = new Rover(x, y, compassDirection);
        
        Assert.AreEqual(compassDirection, _rover.CompassDirection);
        Assert.AreEqual(x, _rover.X);
        Assert.AreEqual(y, _rover.Y);
    }
    
    [Test]
    [TestCase(CompassDirection.North, CompassDirection.West)]
    [TestCase(CompassDirection.South, CompassDirection.East)]
    [TestCase(CompassDirection.East, CompassDirection.North)]
    [TestCase(CompassDirection.West, CompassDirection.South)]
    public void CanTurnLeft(CompassDirection initialCompassDirection, CompassDirection expectedCompassDirection)
    {
        _rover = new Rover(0, 0, initialCompassDirection);

        _rover.Turn(TurnCommand.Left);
        
        Assert.AreEqual(expectedCompassDirection, _rover.CompassDirection);
        Assert.AreEqual(0, _rover.X);
        Assert.AreEqual(0, _rover.Y);
    }

    [Test]
    [TestCase(CompassDirection.North, CompassDirection.East)]
    [TestCase(CompassDirection.South, CompassDirection.West)]
    [TestCase(CompassDirection.East, CompassDirection.South)]
    [TestCase(CompassDirection.West, CompassDirection.North)]
    public void CanTurnRight(CompassDirection initialCompassDirection, CompassDirection expectedCompassDirection)
    {
        _rover = new Rover(0, 0, initialCompassDirection);
        
        Assert.AreEqual(initialCompassDirection, _rover.CompassDirection);
        
        _rover.Turn(TurnCommand.Right);
        
        Assert.AreEqual(expectedCompassDirection, _rover.CompassDirection);
        Assert.AreEqual(0, _rover.X);
        Assert.AreEqual(0, _rover.Y);
    }
    
    [Test]
    [TestCase(0, 0, CompassDirection.North, 0, 1)]
    [TestCase(0, 0, CompassDirection.East, 1, 0)]
    [TestCase(0, 0, CompassDirection.West, -1, 0)]
    [TestCase(0, 0, CompassDirection.South, 0, -1)]
    public void CanMoveForward(int initialX, int initialY, CompassDirection facingCompassDirection, int expectedX, int expectedY)
    {
        _rover = new Rover(initialX, initialY, facingCompassDirection);
        
        _rover.Move(MovementCommand.Forward);
        
        Assert.AreEqual(expectedX, _rover.X);
        Assert.AreEqual(expectedY, _rover.Y);
        
        // Expect direction did not change.
        Assert.AreEqual(facingCompassDirection, _rover.CompassDirection);
    }
    
    [Test]
    [TestCase(0, 0, CompassDirection.North, 0, -1)]
    [TestCase(0, 0, CompassDirection.East, -1, 0)]
    [TestCase(0, 0, CompassDirection.West, 1, 0)]
    [TestCase(0, 0, CompassDirection.South, 0, 1)]
    public void CanMoveBackward(int initialX, int initialY, CompassDirection facingCompassDirection, int expectedX, int expectedY)
    {
        _rover = new Rover(initialX, initialY, facingCompassDirection);
        
        _rover.Move(MovementCommand.Backward);
        
        Assert.AreEqual(expectedX, _rover.X);
        Assert.AreEqual(expectedY, _rover.Y);
        
        // Expect direction did not change.
        Assert.AreEqual(facingCompassDirection, _rover.CompassDirection);
    }
}
