﻿using System;
using NUnit.Framework;
using Somafet.Rover.Impl;

namespace Somafet.Rover.Tests;

using Impl.Utils;
using Impl.Models;

public class RoverControllerTest
{
    private Rover _rover = null!;
    private RoverController _roverController = null!;
    
    [SetUp]
    public void Setup()
    {
        _rover = new Rover();
        _roverController = new RoverController(_rover);
    }

    [Test]
    [TestCase("FFRFF", 2, 2, CompassDirection.East)]
    [TestCase("FLF", -1, 1, CompassDirection.West)]
    [TestCase("LL", 0, 0, CompassDirection.South)]
    [TestCase("FFFFRFFFFRFFLL", 4, 2, CompassDirection.North)]
    public void CanFollowInputCommand(string input, int expectedX, int expectedY, CompassDirection expectedDirection)
    {
        _roverController.InputCommand(input);
        
        Assert.AreEqual(expectedX, _rover.X);
        Assert.AreEqual(expectedY, _rover.Y);
        Assert.AreEqual(expectedDirection, _rover.CompassDirection);
    }
    
    [Test]
    [TestCase("L2")]
    public void WillThrowErrorWithInvalidInput(string input)
    {
        Assert.Throws<InvalidCommandException>(() => _roverController.InputCommand(input));
    }
}